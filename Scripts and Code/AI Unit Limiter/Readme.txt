Changes the AI change so they only produce 1 divisions per 1 factory, reduces endgame lag by a ton and makes all nations have strong divisions with full equipment instead of just pumping out new ones
Can be individually tweaked per nation if needed

Credit to Gunnar Von Pontius